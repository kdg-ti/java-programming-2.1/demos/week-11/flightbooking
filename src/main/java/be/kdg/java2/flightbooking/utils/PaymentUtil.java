package be.kdg.java2.flightbooking.utils;

import be.kdg.java2.flightbooking.exceptions.InsufficientMoneyException;

public class PaymentUtil {
    public static void checkAccount(String account, double amount) {
        if (amount>1000) throw new InsufficientMoneyException("Amount exceeds " + 1000);
    }
}
