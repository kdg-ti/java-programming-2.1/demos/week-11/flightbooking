package be.kdg.java2.flightbooking.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Destination not available")
public class DestinationNotAvailableException extends RuntimeException {
    private String destination;
    public DestinationNotAvailableException(String destination, String message) {
        super(message);
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }
}
