package be.kdg.java2.flightbooking.service;

import be.kdg.java2.flightbooking.domain.PassengerInfo;
import be.kdg.java2.flightbooking.domain.PaymentInfo;
import be.kdg.java2.flightbooking.exceptions.DestinationNotAvailableException;
import be.kdg.java2.flightbooking.repository.PassengerInfoRepository;
import be.kdg.java2.flightbooking.repository.PayementInfoRepository;
import be.kdg.java2.flightbooking.utils.PaymentUtil;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;

@Service
public class FlightBookingServiceImpl implements FlightBookingService{
    private PassengerInfoRepository passengerInfoRepository;
    private PayementInfoRepository payementInfoRepository;

    public FlightBookingServiceImpl(PassengerInfoRepository passengerInfoRepository, PayementInfoRepository payementInfoRepository) {
        this.passengerInfoRepository = passengerInfoRepository;
        this.payementInfoRepository = payementInfoRepository;
    }

    @Override
    @Transactional
    public void bookFlight(String passengerName, String destination, String account) {
        passengerInfoRepository.save(new PassengerInfo(passengerName, destination));
        if (!getDestinations().containsKey(destination))
            throw new DestinationNotAvailableException(destination, "Destination " + destination + " not available");
        double amount = getDestinations().get(destination);
        PaymentUtil.checkAccount(account, amount);
        payementInfoRepository.save(new PaymentInfo(account, amount));
    }

    @Override
    public Map<String, Double> getDestinations() {
        Map<String, Double> prices = new HashMap<>();
        prices.put("Dubai", 2000.0);
        prices.put("Madrid", 340.0);
        prices.put("New York", 450.0);
        return prices;
    }
}
