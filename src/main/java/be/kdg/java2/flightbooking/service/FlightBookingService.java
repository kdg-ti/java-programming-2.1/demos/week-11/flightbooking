package be.kdg.java2.flightbooking.service;

import java.util.Map;

public interface FlightBookingService {
    void bookFlight(String passengerName, String destination, String account);
    Map<String, Double> getDestinations();
}
