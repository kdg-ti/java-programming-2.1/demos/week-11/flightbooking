package be.kdg.java2.flightbooking.ui.dto;

public class FlightBookingDTO {
    private String name;
    private String destination;
    private String account;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "FlightBookingDTO{" +
                "name='" + name + '\'' +
                ", destination='" + destination + '\'' +
                ", account='" + account + '\'' +
                '}';
    }
}
