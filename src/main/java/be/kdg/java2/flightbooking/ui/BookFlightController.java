package be.kdg.java2.flightbooking.ui;

import be.kdg.java2.flightbooking.exceptions.DestinationNotAvailableException;
import be.kdg.java2.flightbooking.exceptions.InsufficientMoneyException;
import be.kdg.java2.flightbooking.service.FlightBookingService;
import be.kdg.java2.flightbooking.ui.dto.FlightBookingDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class BookFlightController {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private FlightBookingService flightBookingService;

    public BookFlightController(FlightBookingService flightBookingService) {
        this.flightBookingService = flightBookingService;
    }

    @GetMapping({"","/index","/"})
    public String getBookFlightPage(Model model){
        model.addAttribute("destinations", flightBookingService.getDestinations());
        model.addAttribute("flightBooking",new FlightBookingDTO());
        return "index";
    }

    @PostMapping("/bookflight")
    public String bookFlight(FlightBookingDTO flightBookingDTO){
        flightBookingService.bookFlight(flightBookingDTO.getName(), flightBookingDTO.getDestination(), flightBookingDTO.getAccount());
        //flightBookingService.bookFlight(flightBookingDTO.getName(), "Brussels", flightBookingDTO.getAccount());
        return "redirect:index";
    }

    @ExceptionHandler(DestinationNotAvailableException.class)
    public ModelAndView handleError(HttpServletRequest req, DestinationNotAvailableException exception) {
        logger.error(exception.getMessage());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", exception);
        modelAndView.setViewName("destinationerror");
        return modelAndView;
    }
}
